package com.example.hw2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.text.Editable
import android.text.InputType.TYPE_CLASS_TEXT
import android.text.InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.text.method.KeyListener
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() , View.OnClickListener , BottomNavigationView.OnNavigationItemSelectedListener
    , TextWatcher {


    private var saveInputType : String = "bin"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomBar.selectedItemId = R.id.bin
        number.keyListener = DigitsKeyListener.getInstance("01")

        clear.isClickable = false

        clear.setOnClickListener(this)
        bottomBar.setOnNavigationItemSelectedListener (this)
        number.addTextChangedListener(this)

                                                       }//onCreate

    override fun onClick(v: View?) {

        if (v!!.id == clear.id){

            inBinText.text = ""
            inOctText.text = ""
            inDecText.text = ""
            inHexText.text = ""

            number.text.clear()

            numberType.text = "Number Type : Binary"

            saveInputType = "bin"

            bottomBar.selectedItemId = R.id.bin

                               }//if statement (for clear button)

                                   }//onClick

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {

        when {

            p0.itemId == R.id.bin -> {

                numberType.text = "Number Type : Binary"

                number.keyListener = DigitsKeyListener.getInstance("01")

                when (saveInputType) {

                    "oct" -> {

                        saveInputType = "bin"

                        if (!number.text.isEmpty())

                            number.text = Editable.Factory.getInstance()
                                .newEditable(java.lang.Long.toBinaryString(java.lang.Long.parseLong(number.text.toString(), 8)))



                               }//Octal Number Case

                    "dec" -> {

                        saveInputType = "bin"

                        if (!number.text.isEmpty())

                           number.text = Editable.Factory.getInstance()
                                .newEditable(java.lang.Long.toBinaryString(java.lang.Long.parseLong(number.text.toString())))



                              }//Decimal Number Case

                    "hex" -> {

                        saveInputType = "bin"

                        if (!number.text.isEmpty())

                            number.text = Editable.Factory.getInstance()
                                .newEditable(java.lang.Long.toBinaryString(java.lang.Long.parseLong(number.text.toString(), 16)))



                              }//Hexadecimal Number Case

                                     }//when statement

                                       }//for Binary Selection

            p0.itemId == R.id.oct -> {

                numberType.text = "Number Type : Octal"

                number.keyListener = DigitsKeyListener.getInstance("01234567")

                when (saveInputType){

                    "bin" -> {

                        saveInputType = "oct"

                        if (!number.text.isEmpty())

                            number.text = Editable.Factory.getInstance()
                                .newEditable(java.lang.Long.toOctalString(java.lang.Long.parseLong(number.text.toString(), 2)))



                             }//Binary Number Case

                    "dec" -> {

                        saveInputType = "oct"

                        if (!number.text.isEmpty())

                            number.text = Editable.Factory.getInstance()
                                .newEditable(java.lang.Long.toOctalString(java.lang.Long.parseLong(number.text.toString())))



                              }//Decimal Number Case

                    "hex" -> {

                        saveInputType = "oct"

                        if (!number.text.isEmpty())

                            number.text = Editable.Factory.getInstance()
                                .newEditable(java.lang.Long.toOctalString(java.lang.Long.parseLong(number.text.toString(), 16)))

                              }//Hexadecimal Number Case

                                    }//when statement

                                      }//for Octal Selection

            p0.itemId == R.id.dec -> {

                numberType.text = "Number Type : Decimal"

                number.keyListener = DigitsKeyListener.getInstance("0123456789")

                when (saveInputType) {

                    "bin" -> {

                        saveInputType = "dec"

                        if (!number.text.isEmpty())

                            number.text = Editable.Factory.getInstance()
                                .newEditable("" + java.lang.Long.parseLong(number.text.toString(), 2))

                              }//Binary Number Case

                    "oct" -> {

                        saveInputType = "dec"

                        if (!number.text.isEmpty())

                            number.text = Editable.Factory.getInstance()
                                .newEditable("" + java.lang.Long.parseLong(number.text.toString(), 8))

                              }//Octal Number Case

                    "hex" -> {

                        saveInputType = "dec"

                        if (!number.text.isEmpty())

                            number.text = Editable.Factory.getInstance()
                                .newEditable("" + java.lang.Long.parseLong(number.text.toString(), 16))

                              }//Hexadecimal Number Case

                                     }//when statement


                                      }//for Decimal Selection

            p0.itemId == R.id.hex -> {

                numberType.text = "Number Type : Hexadecimal"

                number.keyListener = DigitsKeyListener.getInstance("0123456789abcdef")

                when (saveInputType) {

                    "bin" -> {

                        saveInputType = "hex"

                        if (!number.text.isEmpty())

                            number.text = Editable.Factory.getInstance()
                                .newEditable(java.lang.Long.toHexString(java.lang.Long.parseLong(number.text.toString(), 2)))

                              }//Binary Number Case

                    "oct" -> {

                        saveInputType = "hex"

                        if (!number.text.isEmpty())

                            number.text = Editable.Factory.getInstance()
                                .newEditable(java.lang.Long.toHexString(java.lang.Long.parseLong(number.text.toString(), 8)))

                              }//Octal Number Case

                    "dec" -> {

                        saveInputType = "hex"

                        if (!number.text.isEmpty())

                            number.text = Editable.Factory.getInstance()
                                .newEditable(java.lang.Long.toHexString(java.lang.Long.parseLong(number.text.toString())))

                               }//Decimal Number Case

                                     }//when statement


                                      }//for Hexadecimal Selection


             }//when Statement


        return true
                                                                 }//onNavigationItemSelected

    override fun afterTextChanged(s: Editable?) {}//afterTextChanged

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}//beforeTextChanged

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        if (!s.toString().isEmpty()){

            clear.isClickable = true

            when (saveInputType) {

                "bin" -> {

                    inBinText.text = number.text

                    inOctText.text = java.lang.Long.toOctalString(java.lang.Long.parseLong(number.text.toString(), 2))

                    inDecText.text = java.lang.Long.parseLong(number.text.toString(), 2).toString()

                    inHexText.text = java.lang.Long.toHexString(java.lang.Long.parseLong(number.text.toString(), 2))

                          }//when binary is selected

                "oct" -> {

                    inBinText.text = java.lang.Long.toBinaryString(java.lang.Long.parseLong(number.text.toString(),8))

                    inOctText.text = number.text

                    inDecText.text = java.lang.Long.parseLong(number.text.toString(), 8).toString()

                    inHexText.text = java.lang.Long.toHexString(java.lang.Long.parseLong(number.text.toString(), 8))

                         }//when octal is selected

                "dec" -> {

                    inBinText.text = java.lang.Long.toBinaryString(java.lang.Long.parseLong(number.text.toString()))

                    inOctText.text = java.lang.Long.toOctalString(java.lang.Long.parseLong(number.text.toString()))

                    inDecText.text = number.text

                    inHexText.text = java.lang.Long.toHexString(java.lang.Long.parseLong(number.text.toString()))

                         }//when decimal is selected

                "hex" -> {

                    inBinText.text = java.lang.Long.toBinaryString(java.lang.Long.parseLong(number.text.toString(), 16))

                    inOctText.text = java.lang.Long.toOctalString(java.lang.Long.parseLong(number.text.toString(), 16))

                    inDecText.text = java.lang.Long.parseLong(number.text.toString(), 16).toString()

                    inHexText.text = number.text

                          }//when hexadecimal is selected

                                  }//when statement

                                    }//if statement

        else if (s.toString().isEmpty()){

            clear.callOnClick()
            clear.isClickable = false

                                        }//else if statement

                                                                                      }//onTextChanged

                }//mainActivity